;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;      IDL Final Project               ;
;         BLACKJACK                    ;
;   Dominic LeDuc and Allen Ruan       ;
;                                      ;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


;DEALING TWO CARDS OUT
function deal
  probab = fix((randomu(seed, 2)*13)+1) ;creates a 2-element array of random integers from 1-13

  player_hand = list() ;creates list to hold the player's two cards
  card_number = []

  first_card = probab(0) ;first and second elements of the random integers
  second_card = probab(1)

  ;defines the structure of the face cards
  Jack = {name: 'J', value: 10}
  Queen = {name: 'Q', value: 10}
  King = {name: 'K', value: 10}
  Ace = {name: 'A', value: 11}

  ;Creates player_hand (the array of two cards)
  for i = 0, 1 do begin
     case 1 of
        first_card EQ 1: card_number = Ace.name
        first_card EQ 2: card_number = 2
        first_card EQ 3: card_number = 3
        first_card EQ 4: card_number = 4
        first_card EQ 5: card_number = 5
        first_card EQ 6: card_number = 6
        first_card EQ 7: card_number = 7
        first_card EQ 8: card_number = 8
        first_card EQ 9: card_number = 9
        first_card EQ 10: card_number = 10
        first_card EQ 11: card_number = Jack.name
        first_card EQ 12: card_number = Queen.name
        first_card EQ 13: card_number = King.name
     endcase

     player_hand = player_hand + list(card_number)
     ; used lists instead of arrays because there are different datatypes involved
     first_card = second_card
     ; so that for-loop can run through again using the second card's value
  endfor

  return, player_hand
end



; FIRST DEAL--you get two cards but you only see one of the dealer's cards
function first_round, player_hand, cpu_hand

  player_hand_strings = []
  for i = 0, 1 do begin ; runs through and converts integers to strings to print out nicely below
    if typename(player_hand(i)) NE 'STRING' then begin
      number_as_string = string(player_hand[i])
      player_hand_strings = [player_hand_strings, number_as_string]
    endif else begin
      if typename(player_hand[i]) EQ 'STRING' then begin
        player_hand_strings = [player_hand_strings, player_hand[i]]
      endif else begin
        print, 'Invalid! Type better.'
      endelse
    endelse
  endfor


  print, ''
  print, '------------------------------------------------------------------'
  print, 'Your Hand: ', player_hand_strings
  print, '------------------------------------------------------------------'
  print, ''

  ;Repeats the exact same procedures except for the CPU
  cpu_hand_strings = []
  for i = 0, 1 do begin
    if typename(cpu_hand(i)) NE 'STRING' then begin
      number_as_string = string(cpu_hand[i])
      cpu_hand_strings = [cpu_hand_strings, number_as_string]
    endif else begin
      if typename(cpu_hand[i]) EQ 'STRING' then begin
        cpu_hand_strings = [cpu_hand_strings, cpu_hand[i]]
      endif else begin
        print, 'Invalid! Type better.'
      endelse
    endelse
  endfor

  print, ''
  print, '------------------------------------------------------------------'
  print, 'Top Card of Dealer: ', cpu_hand_strings[0]
  print, '------------------------------------------------------------------'
  return, ''

end



; CHECKING THE SCORES CHECKING THE SCORES CHECKING THE SCORES CHECKING THE SCORES
; Function to check the scores of the player's and CPU's hand
function score_check, player_hand, cpu_hand
  ;need this structure defined here again for the case statements below to work
  Jack = {name: 'J', value: 10}
  Queen = {name: 'Q', value: 10}
  King = {name: 'K', value: 10}
  Ace = {name: 'A', value: 11}

  PLAYER_face_value = [] ; will be used to store the numerical value of the cards

  for i = 0, n_elements(player_hand)-1 do begin
    ; runs loop for each card in the player's hand
    ; loop will take the numerical value of each card and put them into PLAYER_face_value
     if typename(player_hand(i)) EQ 'STRING' then begin
    ; used to check whether card is a face card (typename(player_hand(i)) would return a value of 'string')
       case player_hand(i) of
        'J': PLAYER_face_value = [PLAYER_face_value, Jack.value]
        'Q': PLAYER_face_value = [PLAYER_face_value, Queen.value]
        'K': PLAYER_face_value = [PLAYER_face_value, King.value]
        'A': PLAYER_face_value = [PLAYER_face_value, Ace.value]
        ; adds the numerical value of the face card to PLAYER_face_value
        else: return, 'Delete this foul string you have inputted in the player hand'
       endcase
     endif else begin
       if typename(player_hand(i)) EQ 'INT' then begin
         PLAYER_face_value = [PLAYER_face_value, player_hand(i)]
         ; merely adds the value of the card to the face value array
      endif else begin
          return, "All right. Something went whack around here. Input only strings or integers." ;protection against bugs
      endelse
    endelse
  endfor



  ; does the same thing as above code, but for the CPU now
  CPU_face_value = []

  for i = 0, n_elements(cpu_hand)-1 do begin
     if typename(cpu_hand(i)) EQ 'STRING' then begin
       case cpu_hand(i) of
        'J': CPU_face_value = [CPU_face_value, Jack.value]
        'Q': CPU_face_value = [CPU_face_value, Queen.value]
        'K': CPU_face_value = [CPU_face_value, King.value]
        'A': CPU_face_value = [CPU_face_value, Ace.value]
        else: return, "Delete this foul string you have inputted to CPU's hand"
       endcase
     endif else begin
       if typename(cpu_hand(i)) EQ 'INT' then begin
         CPU_face_value = [CPU_face_value, cpu_hand(i)]
       endif else begin
         return, "All right. Something went whack around here. Input only strings or integers."
       endelse
     endelse
  endfor


  ;=============VARIABLE ACES=============
  ;This is to account for the fact that aces can have a value of either 1 or 11
  ;Lists --> String arrays: This is taken from the function STRINGER.
  Pcards_as_strings = []
  CPUcards_as_strings = []

  for i = 0, n_elements(player_hand)-1 do begin ;converts the player's hand into strings
      number_as_stringp = string(player_hand[i])
      Pcards_as_strings = [Pcards_as_strings, number_as_stringp]
  endfor

  for i = 0, n_elements(cpu_hand)-1 do begin ;converts the CPU's hand into strings
      number_as_stringc = string(cpu_hand[i])
      CPUcards_as_strings = [CPUcards_as_strings, number_as_stringc]
  endfor

  ;Determining how many Aces are in each hand
  Area_of_acesP = where(strmatch(Pcards_as_strings, 'A')) ;returns an array of the in\dices that display the Ace or not ace
  Area_of_acesC = where(strmatch(CPUcards_as_strings, 'A'))

  ;Adjusts so that the variability only affects Aces and not other cards
  for i = 0, n_elements(Area_of_acesP)-1 do begin
     if Area_of_acesP(i) GE 0 then begin
        num_of_acesP = n_elements(Area_of_acesP)
     endif else begin
        num_of_acesP = 0
     endelse
  endfor
  ;Repeats the following, except for the CPU
  for i = 0, n_elements(Area_of_acesC)-1 do begin
     if Area_of_acesC(i) GE 0 then begin
        num_of_acesC = n_elements(Area_of_acesC)
     endif else begin
        num_of_acesC = 0
     endelse
  endfor

  ;Calculates current scores of player's hands
  p_score = fix(total(PLAYER_face_value))
  cpu_score = fix(total(CPU_face_value))

  ;Adjusting the score for both the player and the CPU
  for i = 0, num_of_acesP-1 do begin
     if p_score GT 21 then begin
        p_score = p_score - 10
     endif else begin
        p_score = p_score
     endelse
  endfor

  for i = 0, num_of_acesC-1 do begin
     if cpu_score GT 21 then begin
        cpu_score = cpu_score - 10
     endif else begin
        cpu_score = cpu_score
     endelse
  endfor

   ;========END OF VARIABLE ACES============

  return, [p_score, cpu_score] ;returns an array containing the player's and CPU's current scores
end



; Converts all items in player and CPU hands to string types
; presents them to player in hopefully good formatting
function stringer, player_hand, cpu_hand

  ;calling the score_check function to evaluate the current hands
  scores = score_check(player_hand, cpu_hand)
  p_score = scores(0)
  cpu_score = scores(1)

  ;Sets up the formatting to show what the player's hand currently looks like
  Pcards_as_strings = []
  for i = 0, n_elements(player_hand)-1 do begin ; converts every item in the list into a string, for nice formatting purpose when returning in IDL
     number_as_string = string(player_hand[i])
     Pcards_as_strings = [Pcards_as_strings, number_as_string]
  endfor

  ; '---' creates a barrier and '' is a blank line to make it look prettier and more spaced out
  print, ''
  print, '------------------------------------------------------------------'

  print, 'In your hand you have: ', Pcards_as_strings
  print, 'which totals up to: ', p_score

  print, '------------------------------------------------------------------'
  print, ''

  ;Sets up the formatting to show what the CPU's hand looks like during the player's turn
  CPUcards_as_strings = []
  for i = 0, n_elements(cpu_hand)-1 do begin
     number_as_string = string(cpu_hand[i])
     CPUcards_as_strings = [CPUcards_as_strings, number_as_string]
  endfor
  
  ;We are only allowed to see the CPU's top card, as in an actual game of blackjack
  print, '------------------------------------------------------------------'
  print, "Your opponent's top card is: ", CPUcards_as_strings(0)
  print, '------------------------------------------------------------------'
  return, ''
end


;FUNCTION SIMILAR TO STRINGER, EXCEPT FOR THE LAST ROUND ONLY
;The purpose of this function is to reveal everyone's cards
;when the game ends
function last_stringer, player_hand, cpu_hand
  scores = score_check(player_hand, cpu_hand)
  p_score = scores(0)
  cpu_score = scores(1)

  ;The player's hand
  Pcards_as_strings = []
  for i = 0, n_elements(player_hand)-1 do begin ; converts every item in the list into a string, for nice formatting purpose when returning in IDL
     number_as_string = string(player_hand[i])
     Pcards_as_strings = [Pcards_as_strings, number_as_string]
  endfor

  ; '---' creates a barrier and '' is a blank line to make it look prettier and more spaced out
  print, ''
  print, '------------------------------------------------------------------'
  print, 'In your hand you have: ', Pcards_as_strings
  print, 'which totals up to: ', p_score
  print, '------------------------------------------------------------------'
  print, ''

  ;The CPU's hand
  CPUcards_as_strings = []
    for i = 0, n_elements(cpu_hand)-1 do begin
      number_as_string = string(cpu_hand[i])
      CPUcards_as_strings = [CPUcards_as_strings, number_as_string]
   endfor

  print, '------------------------------------------------------------------'
  print, 'Your opponent has: ', CPUcards_as_strings
  print, 'which totals up to: ', cpu_score
  print, '------------------------------------------------------------------'
  return, ''

end



;HIT ME: Function that will append another card to the input hand and
;returns the new hand
function hit, hand

  ;Selecting a random card to append
  hit_card = fix((randomu(seed, 1)*13)+1)

  card_number = []

  Jack = {name: 'J', value: 10}
  Queen = {name: 'Q', value: 10}
  King = {name: 'K', value: 10}
  Ace = {name: 'A', value: 11}

     case 1 of
        hit_card EQ 1: card_number = Ace.name
        hit_card EQ 2: card_number = 2
        hit_card EQ 3: card_number = 3
        hit_card EQ 4: card_number = 4
        hit_card EQ 5: card_number = 5
        hit_card EQ 6: card_number = 6
        hit_card EQ 7: card_number = 7
        hit_card EQ 8: card_number = 8
        hit_card EQ 9: card_number = 9
        hit_card EQ 10: card_number = 10
        hit_card EQ 11: card_number = Jack.name
        hit_card EQ 12: card_number = Queen.name
        hit_card EQ 13: card_number = King.name
     endcase

     hand = hand + list(card_number)

  return, hand
end


;AI DECISION FUNCTION: Function that controls how the CPU will respond
;to the player's hand
function cpu_decide, player_hand, cpu_hand ;input both the player's and the CPU's hand

  ;Reevaluating the current score of the player (after the player has finished)
  battle_score = score_check(player_hand, cpu_hand)
  p_score = battle_score(0)
  cpu_score = battle_score(1)

  ;How the CPU will respond based on the player's current score
  while cpu_score LT p_score do begin
     if cpu_score LT p_score then begin
        cpu_hand = hit(cpu_hand)   ;CPU will hit if its score is lower than player's
     endif else begin
        if cpu_score EQ p_score then begin
           cpu_hand = cpu_hand ;CPU will stay if its score is the same
        endif else begin
           if cpu_score GT p_score then begin
              cpu_hand = cpu_hand ;CPU will stay if its score is higher than player's
           endif else begin
              print, 'Something went wrong here...' ;protection against bugs
              print, ''
           endelse
        endelse
     endelse

     ;return the new scores
     scores = score_check(player_hand, cpu_hand)
     p_score = scores(0)
     cpu_score = scores(1)

  endwhile

  return, cpu_hand ;return the computer's final hand
end








;================THE MAIN SCRIPT====================
pro blackjack

  ;Setting up the replay functionality
  replay = 'ON'

  ;Large while loop wrapping around the whole procedure
  while replay EQ 'ON' do begin ;while loop to allow replay functionality

     print, ' _____  _______________________  _____  '
     print, ' _____                           _____  '
     print, '|10  ^| ~---------------------~ |A .  | '
     print, '|^ ^ ^| |                     | | /.\ | '
     print, "|^ ^ ^| |Welcome to blackjack!| |(_._)| "
     print, '|^ ^ ^| |                     | |  |  | '
     print, '|___0I| ~---------------------~ |____V| '
     print, ' _____  _______________________  _____  '
     print, ''

  ;========ASK IF WANT TO PLAY (Y/N)===========
     valid = 0
     while valid EQ 0 do begin ; if person enters anything other than Y or N, then loop around until valid input is recognized
        response = ''
        read, response, prompt = 'Would you like to begin? (Y/N)'

        if response EQ 'N' then begin ;if the player decides to not play
           print, ''
           print, 'Wussing out against the dealer, I see.'
           return
        endif else begin
           if response EQ 'Y' then begin ;If the player decides to play, begin!
              BREAK
           endif else begin ;If player inputs a lowercase letter
              if response EQ 'y' or response EQ 'n' then begin
                 print, ''
                 print, 'Capitalize that shit.'
              endif else begin ;if player types anything else
                 print, ''
                 print, 'Not a valid input. Type either Y or N, and only Y and N'
              endelse
           endelse
        endelse
     endwhile

  ;==========DEAL TWO CARDS==============
  ;DEAL CARDS OUT AUTOMATICALLY: Player should be able to see own two cards and top card of the CPU
  ;Use function deal
     player_hand = deal()
     cpu_hand = deal()

     print, first_round(player_hand, cpu_hand)

     scores = score_check(player_hand, cpu_hand) ; checks player and CPU numerical score
     p_score = scores(0)
     cpu_score = scores(1)

     ;If the player gets blackjack in the first deal
     if p_score EQ 21 then begin
        print, ''
        print, ''
        print, ''
        print, '---------------------------------------------------------------'
        print, '---------------------------------------------------------------'
        print, '---------------------------------------------------------------'
        print, '---------------------------------------------------------------'
        print, '---------------------------------------------------------------'
        print, 'DING DING DING! BLACKJAAAAAAAAAAACK! CONGRATZ ON WINNING YOU LUCKY DUCK!!!!!!'
        print, '---------------------------------------------------------------'
        print, '---------------------------------------------------------------'
        print, '---------------------------------------------------------------'
        print, '---------------------------------------------------------------'
        print, '---------------------------------------------------------------'
        print, ''
        print, ''
        print, ''
        BREAK
     endif


    ;========HIT OR STAY OPTION=========
    ;Stop, and offer choice to "hit," or "stay"
    ;If hit, show three + cards in array; if too high --> END GAME NOW
     wait, 0.5  ; pauses for 1.5 seconds before running through the next part of the program
     hit_stay = 'hit'
     while hit_stay EQ 'hit' do begin ; while loop will keep taking inputs until valid input(hit/stay) is recognized

        read, hit_stay, prompt = 'WILL YOU HIT OR WILL YOU STAY? (Type hit or stay)' ; takes in whether player wants to hit or to stay

        ;Player decides to HIT
        if hit_stay EQ 'hit' then begin
           player_hand = hit(player_hand) ; will append another card to player_hand
           scores = score_check(player_hand, cpu_hand) ; checks player and CPU numerical score
           ;The current player's and CPU's scores
           p_score = scores(0)
           cpu_score = scores(1)

           ; All the different types of conditionals
           if p_score GT 21 then begin ;If the player BUSTS with the hit
              print, last_stringer(player_hand, cpu_hand)
              print, 'KABOOOOOOM!!!! You busted!'
              print, 'Sorry, your score is over 21, so you lost!'
              print, 'Better luck (or perhaps skill) next time!'
              print, ''
              BREAK
           endif else begin
              if p_score EQ 21 then begin ;If the player got a 21 with a hit
                 print, last_stringer(player_hand, cpu_hand)
                 print, ''
                 print, ''
                 print, ''
                 print, '---------------------------------------------------------------'
                 print, '---------------------------------------------------------------'
                 print, '---------------------------------------------------------------'
                 print, '---------------------------------------------------------------'
                 print, '---------------------------------------------------------------'
                 print, 'DING DING DING! You got exactly 21! CONGRATZ ON WINNING YOU LUCKY DUCK!!!!!!'
                 print, '---------------------------------------------------------------'
                 print, '---------------------------------------------------------------'
                 print, '---------------------------------------------------------------'
                 print, '---------------------------------------------------------------'
                 print, '---------------------------------------------------------------'
                 print, ''
                 print, ''
                 print, ''
                 BREAK
              endif else begin
                 if cpu_score EQ 21 then begin ;If the CPU gets blackjack
                    print, last_stringer(player_hand, cpu_hand)
                    print, ''
                    print, ''
                    print, ''
                    print, '---------------------------------------------------------------'
                    print, '---------------------------------------------------------------'
                    print, '---------------------------------------------------------------'
                    print, '---------------------------------------------------------------'
                    print, '---------------------------------------------------------------'
                    print, 'BLACKJAAAAAAAAAAACK... for the cpu, that is. lol so you lose'
                    print, '---------------------------------------------------------------'
                    print, '---------------------------------------------------------------'
                    print, '---------------------------------------------------------------'
                    print, '---------------------------------------------------------------'
                    print, '---------------------------------------------------------------'
                    print, ''
                    print, ''
                    print, ''
                    BREAK
                 endif else begin
                    print, stringer(player_hand, cpu_hand) ;prints the dialogue of whats in your hand
                 endelse
              endelse
           endelse
        endif else begin

           ;If the player chooses the STAY option
           if hit_stay EQ 'stay' then begin

              ;Reevaluating the scores after the player finishes
              scores = score_check(player_hand, cpu_hand)
              p_score = scores[0]
              cpu_score = scores[1]

              ;All the possible conditionals
              if p_score GT 21 then begin
                 print, last_stringer(player_hand, cpu_hand)
                 print, 'KABOOOOOM!!! You busted!'
                 print, 'Sorry, your score is over 21, so you lost!'
                 print, 'Better luck (or perhaps skill) next time!'
                 print, ''
              endif else begin
                 ; Incorporates the CPU's decisions
                 cpu_hand = cpu_decide(player_hand, cpu_hand)
                 scores = score_check(player_hand, cpu_hand)
                 cpu_score = scores[1]
                 if cpu_score GT 21 then begin ;If the CPU busts
                    print, last_stringer(player_hand, cpu_hand)
                    print, 'Bingo! The CPU busted!'
                    print, 'Congrats! You won!!'
                    print, ''
                 endif else begin
                    if cpu_score EQ 21 then begin ;if the CPU gets blackjack
                       print, last_stringer(player_hand, cpu_hand)
                       print, 'BLAAAAAAAAACKJAAAAAAAAACK... for the dealer, that is.'
                       print, 'Sorry, pal. Better luck next time.'
                       print, ''
                    endif else begin
                       if p_score GT cpu_score then begin ;if the CPU gets a lower score than the player
                          print, last_stringer(player_hand, cpu_hand)
                          print, 'Since your score is higher,'
                          print, 'We have a WINNER WINNER CHICKEN DINNER!!!!!!'
                          print, 'Congrats champ! You take all the moola.'
                          print, ''
                       endif else begin
                          if cpu_score GT p_score then begin ;if the CPU > Player score
                             print, last_stringer(player_hand, cpu_hand)
                             print, 'Sorry, bucko. The house wins.'
                             print, 'You got creamed. :('
                             print, ''
                          endif else begin ;any other possibility
                             print, last_stringer(player_hand, cpu_hand)
                             print, 'TIE Game!'
                             print, ''
                          endelse
                       endelse
                    endelse
                 endelse
              endelse
           endif else begin ;protection against bugs
              print, 'Invalid input'
              hit_stay = 'hit'
           endelse
        endelse
     endwhile

     ;ASKING PLAYER TO REPLAY GAME
     valid_2 = 0 ;turns switch on

     ;while loop to replay game
     while valid_2 EQ 0 do begin
        replay_choice = ''
        read, replay_choice, prompt = 'Replay this awesome game? (Y/N)'

        if replay_choice EQ 'Y' then begin
           BREAK ;Replays entire procedure
        endif else begin
           if replay_choice EQ 'N' then begin
              print, 'Come again!'
              return ;ends the procedure
           endif else begin
              print, 'Please try again. Y and N only, please.'
           endelse
        endelse
     endwhile
  endwhile


end
